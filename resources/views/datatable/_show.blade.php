{!! Form::model($model, ['url' => $show_url, 'method' => 'get', 'class' => 'form-inline'] ) !!}
{!! Form::button('<i class="fas fa-eye" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-sm btn-info', 'data-toggle' => 'tooltip', 'title' => 'Show')) !!}
{!! Form::close() !!}