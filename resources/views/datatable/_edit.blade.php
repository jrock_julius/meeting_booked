{!! Form::model($model, ['url' => $edit_url, 'method' => 'get', 'class' => 'form-inline'] ) !!}
{!! Form::button('<i class="fas fa-edit" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-sm btn-warning', 'data-toggle' => 'tooltip', 'title' => 'Edit')) !!}
{!! Form::close() !!}
