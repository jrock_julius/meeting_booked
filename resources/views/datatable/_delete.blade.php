{!! Form::model($model, ['url' => $delete_url, 'method' => 'delete', 'class' => 'form-inline js-confirm', 'data-confirm' => $confirm_message] ) !!} 
{!! Form::button('<i class="fas fa-trash" aria-hidden="true"></i>', array('type' => 'submit', 'class' => 'btn btn-sm btn-danger', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
{!! Form::close() !!}