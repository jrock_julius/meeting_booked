@extends('adminlte::page')

@section('title', 'Room - Meeting Booking')

@section('content_header')
    <h1>Room</h1>
@stop

@section('content')
<div class="right_col" role="main">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-6 align-self-center">
        <h4 class="c-grey-900 mT-10 mB-30">Management Room</h4> </div>
        <div class="col-md-6 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">MASTER</li>
                <li class="breadcrumb-item active">Room</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    
    @if(session()->has('flash_notification.message'))
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="body">
                @if(session()->get('flash_notification.level') == 'danger')
                <div style="background-color:red" class="alert alert-{{ session()->get('flash_notification.level') }} alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><font color="white">{!! session()->get('flash_notification.message') !!}</font></strong>
                </div>
                @else
                <div class="alert alert-{{ session()->get('flash_notification.level') }} alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <strong><font color="white">{!! session()->get('flash_notification.message') !!}</font></strong>
                </div>
                @endif
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card card-default color-palette-box">
                <div class="card-body">
                    <div class="table-responsive">
                        {!! $dataTable->table(['class' => 'display nowrap table table-hover table-striped', 'width' => '100%']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    {!! $dataTable->scripts() !!}
    <!-- <script> console.log('Hi!'); </script> -->
@stop