<div class="row p-t-20">
    <div class="col-md-2">
    <div class="form-group{{ $errors->has('room_name') ? 'has-error': '' }} ">
            {!! Form::label('room_name', 'Room Name', ['class'=>'form-label']) !!}
            {!! Form::text('room_name', null, ['class'=>'form-control']) !!} 
            {!! $errors->first('room_name', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group{{ $errors->has('room_capacity') ? 'has-error': '' }} ">
            {!! Form::label('room_capacity', 'Capacity Room', ['class'=>'form-label']) !!}
            {!! Form::text('room_capacity', null, ['class'=>'form-control']) !!} 
            {!! $errors->first('room_capacity', '<small class="text-danger">:message</small>') !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group form-float{{ $errors->has('image_pf') ? 'has-error' : '' }}">
        {!! Form::label('image_pf','Room Pic', ['class'=> 'form-label']) !!}
        {!! Form::file('image_pf'); !!}
        {!! $errors->first('image_pf', '<p class="error">:message</p>') !!}
        </div>
    </div>
</div>

{!! Form::submit ('Simpan',['class'=>'btn btn-primary m-b-10 m-l-5']) !!}