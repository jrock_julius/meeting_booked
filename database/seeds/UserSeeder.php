<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role = Role::where('name','admin')->first();  
        if (!$role) {
            $suRole = new Role();
            $suRole->name = "admin";
            $suRole->display_name = "admin";
            $suRole->save();

            $user = new User();
            $user->email = "julius.setiady@gmail.com";
            $user->password = bcrypt('password');
            $user->save();

            $user->attachRole($suRole);
        }
        $role = Role::where('name','guest')->first();
        if (!$role) {
            $guRole = new Role();
            $guRole->name = "guest";
            $guRole->display_name = "guest";
            $suRole->save();

            $user = new User();
            $user->email = "setiady.jati@hotmail.com";
            $user->password = bcrypt('password');
            $user->save();

            $user->attachRole($guRole);
        }
    }
}
