<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\mRoomDataTable;
use App\mRoom;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class mRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(mRoomDataTable $dataTable)
    {
        //
        return $dataTable->render('master.room.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('master.room.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $folder = 'images/rooms';
        $uploaded = 0;

        $file = $request->image_pf;

        $filename = $file->getClientOriginalName();
        
        $ext = strtolower($file->getClientOriginalExtension());

        $document = md5($filename. $request->room_name. Carbon::today()->toDateTimeString()).'.'. $ext;

        $upload_path = $file->storeAs($folder, $document);

        $this->validate($request , [
            'room_name'    => 'required|unique:m_rooms,room_name',
            'room_capacity'    => 'required',
        ]);

        $room = mRoom::create([
            'room_name'    => $request->room_name,
            'room_capacity'    => $request->room_capacity,
            'photo' => $upload_path,
        ]);

        Session::flash("flash_notification",[
            "level"     => "success",
            "message"   => "Berhasil menyimpan $room->room_name"
        ]);

        return redirect()->route('room.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
