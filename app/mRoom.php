<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class mRoom extends Model implements AuditableContract
{
    //
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'room_name',
        'room_capacity',
        'photo',
    ];
}
