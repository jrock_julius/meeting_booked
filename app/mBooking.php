<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class mBooking extends Model implements AuditableContract
{
    //
    use SoftDeletes;
    use Auditable;

    protected $dates = ['deleted_at'];

    protected $fillable = [
    	'user_id',
        'room_id',
        'total_person',
        'booking_time',
        'noted',
        'check_in_time',
        'check_out_time',
    ];


    public function get_user(){
        return $this->belongsTo('App\User','user_id','id');
    }

    public function get_room(){
        return $this->belongsTo('App\room','room_id','id');
    }
}
