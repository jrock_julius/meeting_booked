<?php

namespace App\DataTables;

use App\mRoom;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class mRoomDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addIndexColumn()
            ->addColumn('edit', function($request){
                return view ('datatable._edit',[
                    'model'    => $request  ,
                    'edit_url' => route('room.edit', $request->id)
                ]);                       
            })

            ->addColumn('delete', function($request){    
                return view ('datatable._delete',[
                    'model'    => $request,
                    'delete_url' => route('room.destroy', $request->id),
                    'confirm_message' => 'Yakin mau menghapus master plant ' . $request->plant_name . '?'
                ]);                 
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\App\mRoom $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(mRoom $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('mroom-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('edit')
                ->exportable(false)
                ->printable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::computed('delete')
                ->exportable(false)
                ->printable(false)
                ->width(10)
                ->addClass('text-center')
                ->title(''),
            Column::make('DT_RowIndex')->orderable(false)->searchable(false)->title('No'),
            Column::make('room_name'),
            Column::make('room_capacity'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'mRoom_' . date('YmdHis');
    }
}
